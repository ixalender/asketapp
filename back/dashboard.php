<?php

require_once("auth.php");

function get_data($key) {
    $result = [$key => "150"];
    return json_encode($result);
}


if (!isset($_GET["token"]) || !auth_check($_GET["token"])) {
    header("HTTP/1.1 401 Unauthorized");
    die;
};

echo get_data($_GET["key"]);