<?php

require_once("auth.php");

function login(string $username, string $password) {
    $result = ["token" => auth($username, $password)];
    return json_encode($result);
}

$json = file_get_contents('php://input');
$data = json_decode($json);
echo login($data->username, $data->password);