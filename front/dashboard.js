require(['./auth'], function (auth) {

  var app = new Vue({
    el: '#app',
    data: {
      test: "1",
      authorized: false
    },
    beforeMount: function () {
      if (auth.checkAuth() == false) {
        window.location.href = "/front/html/login.html";
      }
      this.authorized = auth.checkAuth();
    },
    mounted: function () {
      setTimeout(this.updateData, 10000);
    },
    methods: {
      logout: function () {
        auth.logout();
        this.authorized = false;
      },

      updateData: function () {
        var self = this;
        var token = auth.getToken();

        if (token !== '') {
          axios({
              method: 'get',
              url: '/back/dashboard.php',
              params: {
                key: "test",
                token: token,
              }
            })
            .then(function (response) {
              self.test = response.data.test;
            })
            .catch(function (error) {
              console.log(error);
              UIkit.notification('Error on update data', {
                status: 'danger'
              });
            })
            .then(function () {
              setTimeout(self.updateData, 10000);
            });

        }
      }
    }

  });
});
