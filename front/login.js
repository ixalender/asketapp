require(['./auth'], function(auth) {

  var app = new Vue({
    el: '#app',
    data: {
      errors: [],
      username: undefined,
      password: undefined,
      loggingin: false,
    },
    beforeMount: function () {
      if (auth.checkAuth()) {
        window.location.href = "/front/html/dashboard.html";
      }
    },
    methods: {
      checkForm: function () {
        this.errors = [];

        if (this.username && this.password) {
          return true;
        }

        if (!this.username) {
          this.errors.push('Требуется указать имя.');
        }
        if (!this.password) {
          this.errors.push('Требуется указать пароль.');
        }

      },

      submit: function (e) {
        e.preventDefault();
        var self = this;
        this.loggingin = true;
        if (this.checkForm()) {
          axios({
              method: 'post',
              url: '/back/login.php',
              data: {
                username: this.username,
                password: this.password
              }
            })
            .then(function (response) {
              if (response.data.token === undefined) {
                UIkit.notification('Can`t login', {status:'danger'});
                self.loggingin = false;
                return;
              }
              window.localStorage.setItem("token", response.data.token);
              window.location.href = "/front/html/dashboard.html";
            })
            .catch(function (error) {
              console.log(error);
              UIkit.notification('Can`t login', {status:'danger'});
              self.loggingin = false;
            })
            .then(function () {

            });

        } else {
          this.loggingin = false;
        }
      }
    }

  });
});
