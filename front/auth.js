define([], function () {
  return {
    getToken: function () {
      return window.localStorage.getItem("token");
    },
    checkAuth: function () {
      var token = this.getToken();
      return (
        token !== "" &&
        token !== undefined &&
        token !== null &&
        token !== 'undefined');
    },
    logout: function () {
      window.localStorage.removeItem("token");
      window.location.href = "/front/html/login.html";
    }
  };
});
